export default {
    router: {
        extendRoutes(routes, resolve) {
            routes.push({
                name: 'home',
                path: '/home',
                component: resolve(__dirname, 'pages/home.vue')
            },
            {
                name: 'blog',
                path: '/blog',
                component: resolve(__dirname, 'pages/blog.vue')
            })
        }
    }
}