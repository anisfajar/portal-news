import Vue from 'vue'
import 'swiper/swiper-bundle.css'
import VueAwesomeSwiper from 'vue-awesome-swiper'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import { faSpinner, faStar } from '@fortawesome/free-solid-svg-icons'

library.add(faSpinner, faStar)
Vue.component('icons', FontAwesomeIcon)

Vue.use(VueAwesomeSwiper, /* { default options with global component } */)
