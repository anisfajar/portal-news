import Vue from 'vue'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import VueOnlineProp from "vue-online-prop"
import VueOffline from 'vue-offline'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

import { faSpinner, faStar } from '@fortawesome/free-solid-svg-icons'

library.add(faSpinner, faStar)
Vue.component('icons', FontAwesomeIcon)
Vue.use(VueOnlineProp, VueOffline, BootstrapVue, IconsPlugin)